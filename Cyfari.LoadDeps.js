var Include = function(fileUrl){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {

            var scriptElement = document.createElement("script");
            scriptElement.setAttribute("type", "text/javascript");

        }
    };
    xhttp.open("GET", fileUrl, true);
    xhttp.send();
}
var LoadDeps = function(jsonFile){
    console.log("%cCyDep has begun loading dependencies...", "color: deepskyblue; background-color: lightorange");
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var config = JSON.parse(this.responseText);

            var depBody = document.createElement("CyDep");
            var metaName = document.createElement("CyDep-meta");
            metaName.setAttribute("App-Name", config.appName);
            depBody.appendChild(metaName);
            document.head.appendChild(depBody);

            //load scripts
            console.group("[CyDep] Script Dependencies");
            var i = 0;
            for(var k = 0; i < config.scripts.length; i++) {
                var obj = config.scripts[i];
                var xhttp = new XMLHttpRequest();

                var objname = "" + obj.name;

                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        var scriptElement = document.createElement("script");
                        scriptElement.setAttribute("type", "text/javascript");
                        //scriptElement.setAttribute("CyDep-DepName", config.scripts[i].name);
                        scriptElement.innerHTML = this.responseText;
                        depBody.appendChild(scriptElement);
                    }
                };
                xhttp.open("GET", obj.url, true);
                xhttp.send();
                console.log(obj.name);
            }
            console.groupEnd();

            //Load Stylesheets
            console.group("[CyDep] CSS Dependencies");
            var i = 0;
            for(var k = 0; i < config.stylesheets.length; i++) {
                var obj = config.stylesheets[i];
                var xhttp = new XMLHttpRequest();

                console.log(obj.name);var objname = "" + obj.name;

                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        var scriptElement = document.createElement("link");
                        scriptElement.setAttribute("type", "text/css");
                        scriptElement.setAttribute("rel", "stylesheet");
                        //scriptElement.setAttribute("CyDep-DepName", config.scripts[i].name);
                        scriptElement.innerHTML = this.responseText;
                        depBody.appendChild(scriptElement);
                    }
                };
                xhttp.open("GET", obj.url, true);
                xhttp.send();

            }
            console.groupEnd();
            var scriptElement = document.createElement("script");
            scriptElement.setAttribute("type", "text/javascript");
        }
    };
    xhttp.open("GET", jsonFile, true);
    xhttp.send();

};